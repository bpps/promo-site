<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package GrasshoppHer
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function grasshoppher_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'grasshoppher_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function grasshoppher_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'grasshoppher_pingback_header' );

//Get Social SVGS
add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}

add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
  switch($_REQUEST['fn']){
		case 'get_more_posts':
			$args = [
				'paged' => $_REQUEST['paged'],
				'posts_per_page' => $_REQUEST['count'],
				'post_type' => $_REQUEST['type'],
				'post_status' => 'publish'
			];
			switch($_REQUEST['type']) {
				case 'qa':
					echo json_encode(get_qas($args));
				break;
				case 'mentor':
					echo json_encode(get_mentors($args));
				break;
			}
	  break;
		case 'check_age':
			echo json_encode(check_age());
		break;
		case 'sign_up_user':
			echo signup_user();
		break;
	}
	die();
}

/*-----------------------------*\
  MAILCHIMP
\*-----------------------------*/

//add_action('init', 'mailchimp_sign_up');

include_once 'mailchimp/MailChimp.php';
use \DrewM\MailChimp\MailChimp;

function signup_user(){
	$name = strip_tags( trim( $_REQUEST['name'] ) );
	$email = $_REQUEST['email'];
	$age = intval($_REQUEST['age']);
	$school = strip_tags( trim( $_REQUEST['school'] ) );
  $returnObj = (object)array();
  if (!filter_var($email, FILTER_VALIDATE_EMAIL) || !is_int($age)) {
    //something_went_wrong($email, $result['title']);
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$returnObj->status = 'invalid email';
		}
		if(!is_int($age)) {
			$returnObj->status = 'invalid age';
		}
		return json_encode($returnObj);
  }else{ // Is valid email
    $MailChimp = new MailChimp('d0cd8dd02856ea644cae696bfb8cf139-us18');
    $list_id = '36f41da70b';
		$mcfields = (object)[
			'FULLNAME' => $name,
			'BIRTHDAY' => $_COOKIE["age-verified"],
			'AGE' => $age,
			'SCHOOL' => $school,
		];
    if($MailChimp){
      try {
        $result = $MailChimp->post("lists/$list_id/members", [
    			'email_address' => $email,
    			'status'        => 'subscribed',
					'merge_fields' => $mcfields
    		]);
        $response = '';
        if($result['status'] == '400') {
          switch($result['title']) {
            case 'Member Exists':
              //$response = get_field('already_signed_up', 'option');
							$response = '<h1>Looks like you\'re already signed up!</h1><p>We\'ll keep you updated!</p>';
              $returnObj->response = $response;
              $returnObj->status = 'already signed up';
            break;
            default:
              // $response = get_field('failed', 'option');
							$response = '<h1>Looks like something went wrong!</h1><p>Try again?</p>';
              $returnObj->response = $result;
              $returnObj->status = 'something went wrong';
              //something_went_wrong($email, $result['title']);
            break;
          }
        }else{
          switch($result['status']) {
            case 'subscribed':
              //$response = get_field('success', 'option');
							$response = '<h1>Thanks for signing up!</h1><p>We\'ll keep you updated!</p>';
              $returnObj->response = $response;
              $returnObj->status = 'successful signup';
            break;
          }
        }
        echo json_encode($returnObj);
      } catch (Exception $e) {
        //$returnObj->response = get_field('failed', 'option');
				$returnObj->response = '<h1>Looks like something went wrong!</h1><p>Try again?</p>';
        $returnObj->status = 'invalid email';
        echo json_encode($returnObj);
        //something_went_wrong($email);
      }

    }
  }
}

// function check_age_and_set_cookie($form_data) {
// 	$fields = $form_data['fields'];
// 	foreach( $form_fields as $field ){
// 		if( 'date_1532618035864' == $field[ 'key' ] ){
//     	$birthday = $field['value'];
// 			$expiration = time() + (10 * 365 * 24 * 60 * 60);
// 			setcookie('age-verified', $bday, $expiration, '/');
//   	}
// 	}
// }

function check_age() {
	$expiration = 0;
	$bday = $_REQUEST['bday'];
	//if($_REQUEST['remember'] == 1) {
		$expiration = time() + (10 * 365 * 24 * 60 * 60);
	//}
	setcookie('age-verified', $bday, $expiration, '/');
	return (object)[
		'age' => true,
		'country' => true
	];
}

require get_template_directory() . '/inc/gh-svgs.php';

function gh_social_icons() {
	?>
	<div class="gh-social-icons">
		<div class="gh-social-icons-content">
			<div class="gh-icon" id="icon-fb">
				<a href="https://www.facebook.com/GrasshoppHer-299151483960879" target="_blank">
					<?php social_icon('fb'); ?>
				</a>
			</div>
			<div class="gh-icon" id="icon-youtube">
				<a href="https://www.youtube.com/channel/UCLAr7O5LaV5j8oqPevwUfjA?guided_help_flow=3" target="_blank">
					<?php social_icon('youtube'); ?>
				</a>
			</div>
			<div class="gh-icon" id="icon-twitter">
				<a href="https://twitter.com/GrasshoppHer" target="_blank">
					<?php social_icon('twitter'); ?>
				</a>
			</div>
			<div class="gh-icon" id="icon-ig">
				<a href="https://www.instagram.com/grasshoppher/" target="_blank">
					<?php social_icon('instagram'); ?>
				</a>
			</div>
			<!--<div class="gh-icon" id="icon-snapchat">
				<a href="https://snapchat.com/" target="_blank">
					<?php social_icon('snapchat'); ?>
				</a>
			</div>-->
		</div>
	</div>
	<?php
}

function register_custom_post_types() {

	$labels = array(
    'name' => _x('Q+A', 'post type general name'),
    'singular_name' => _x('Question', 'post type singular name'),
    'add_new' => _x('Add New', 'qa'),
    'add_new_item' => __('Add New Question'),
    'edit_item' => __('Edit Question'),
    'new_item' => __('New Question'),
    'view_item' => __('View Question'),
    'search_items' => __('Search Questions'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies' => array('category'),
  );

  register_post_type( 'qa' , $args );

	$labels = array(
    'name' => _x('Ask a Mentor', 'post type general name'),
    'singular_name' => _x('Mentor', 'post type singular name'),
    'add_new' => _x('Add New', 'qa'),
    'add_new_item' => __('Add New Mentor'),
    'edit_item' => __('Edit Mentor'),
    'new_item' => __('New Mentor'),
    'view_item' => __('View Mentor'),
    'search_items' => __('Search Mentors'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies' => array('category'),
  );

  register_post_type( 'mentor' , $args );

}

add_action('init', 'register_custom_post_types');

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'GrasshoppHer Settings',
		'menu_title'	=> 'GrasshoppHer Settings',
		'menu_slug' 	=> 'grasshoppher-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Footer Settings',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'grasshoppHer-settings',
	// ));

}

function qa_preview() {
	$homeID = get_option('page_on_front');
	$args = [
		'post_type' => 'qa',
		'posts_per_page' => 1,
	];
	$qaText = get_field('qa_text', $homeID);
	$qa_query = new WP_Query( $args );
	if ( $qa_query->have_posts() ) : ?>
		<section id="qa-preview">
			<h3>Ask questions.</h3>
			<div class="qa-preview-text">
				<?php print $qaText; ?>
			</div>
			<?php
			while ( $qa_query->have_posts() ) : $qa_query->the_post(); ?>
				<div id="qa-preview-question" class="mentor-question">
					<?php
					if(has_post_thumbnail()) {
						the_post_thumbnail('small-medium');
					}
					if($qaQuestion = get_field('qa_question')){
						echo $qaQuestion;
					} ?>
				</div>
				<?php
				if( have_rows('responses') ): ?>
					<h6>Some of our favorite responses</h6>
					<div id="preview-responses">
						<?php
						$responseCount = 0;
						while ( have_rows('responses') ) : the_row();
					  	if(get_sub_field('favorite') && $responseCount < 2) {
								$rType = get_sub_field('response_type'); ?>
								<div class="qa-preview-response response-type-<?php echo $rType; ?>">
									<?php
									// switch($rType) {
									// 	case 'ig':
									// 	break;
									// 	case 'twitter':
									// 	break;
									// 	case 'snapchat':
									// 	break;
									// 	case 'text':
									// 	break;
									// 	default:
									// 	break;
									// }
									if($rImg = get_sub_field('response_image')) { ?>
										<div class="qa-response-image-background"></div>
										<div class="qa-response-image bg-centered" style="background-image:url(<?php echo $rImg['sizes']['medium_large']; ?>);">
										</div>
									<?php
									} else {
										if($rText = get_sub_field('response_text')) { ?>

											<?php echo $rText;
										}
									}
									$responseCount++; ?>
								</div>
							<?php
							}
						endwhile; ?>
					</div>
				<?php
				endif; ?>
			<?php
			endwhile; ?>
			<a class="arrow-link" href="<?php echo get_permalink(10); ?>">
				See more
			</a>
		</section>
	<?php
	wp_reset_postdata();
	endif;
}

function mentor_preview() {
	$homeID = get_option('page_on_front');
	$args = [
		'post_type' => 'mentor',
		'posts_per_page' => 1,
		'paged' => 1
	];
	?>
	<?php $mentorText = get_field('ask_a_mentor_text', $homeID); ?>
	<section id="mentor-preview">
		<h3 class="plain-text-header">Get answers.</h3>
		<div class="mentor-preview-text">
			<?php print $mentorText; ?>
		</div>
		<?php
		$mentors = get_mentors($args);
		echo $mentors->posts; ?>
		<a class="arrow-link" href="<?php echo get_permalink(12); ?>">
			See more
		</a>
	</section>
<?php
}

function get_mentors($args) {
	$postsObj = (object)['more' => false];
	$mentor_query = new WP_Query( $args );
	if ( $mentor_query->have_posts() ) :
		while ( $mentor_query->have_posts() ) : $mentor_query->the_post();
			$mentor_link = get_field('mentor_link'); ?>
			<div class="mentor-container">
				<?php
				if(has_post_thumbnail()) {
					$mentor_image = get_the_post_thumbnail_url(get_the_id(), 'small-medium'); ?>
					<div class="mentor-image-container">
						<a target="_blank" href="<?php echo $mentor_link; ?>">
							<div class="mentor-image-background"></div>
							<div class="mentor-image bg-centered" style="background-image:url(<?php echo $mentor_image; ?>);">
							</div>
							<div class="mentor-arrow">
							</div>
						</a>
					</div>
				<?php
				} ?>
				<a class="mentor-link" target="_blank" href="<?php echo $mentor_link; ?>">
					<h6>ask... <span><?php the_title(); ?></span></h6>
				</a>
				<a class="mentor-question" target="_blank" href="<?php echo $mentor_link; ?>">
					<?php the_field('mentor_question'); ?>
				</a>
			</div>
		<?php
		endwhile;
		wp_reset_postdata();
	endif;
	$postsObj->posts = ob_get_clean();
	$postsObj->args = $args;
	$checkArgs = $args;
	$checkArgs['paged'] = isset($checkArgs['paged']) ? $checkArgs['paged'] + 1 : 2;
	$checkNextQuery = new WP_Query( $checkArgs );
	if ( $checkNextQuery->have_posts() ) :
		$postsObj->more = true;
	endif;
	return $postsObj;
}

function get_qas($args) {
	$postsObj = (object)['more' => false, 'posts' => false];
	$qa_query = new WP_Query( $args );
	if ( $qa_query->have_posts() ) :
		ob_start();
		while ( $qa_query->have_posts() ) : $qa_query->the_post();
			$mentor_link = get_field('mentor_link');
			$socialArr = (object)[];
			if($fbQ = get_field('qa_facebook')) {
				$socialArr->facebook = $fbQ;
			}
			if($instaQ = get_field('qa_instagram')) {
				$socialArr->instagram = $instaQ;
			}
			if($twitterQ = get_field('qa_twitter')) {
				$socialArr->twitter = $twitterQ;
			} ?>

			<div class="qa-container new-post">
				<div class="question-header">
					<?php the_title(); ?>
				</div>
				<div class="question-social">
				<?php
				if(count((array)$socialArr) != 0) { ?>

						<div class="instructions">Click here to answer!</div>
						<?php
						$hashtags = get_field('hashtags');
						foreach($socialArr as $key=>$socialLink) { ?>
							<a class="blue-btn share-qa social-<?php echo $key; ?>" target="_blank" href="<?php echo $socialLink; ?>">
								<?php echo $key; ?>
							</a>
						<?php
						} ?>

				<?php
			} ?>
			</div>
			<?php
				if( get_field('responses') ): ?>
					<div class="responses-container">
						<?php
				    foreach( get_field('responses') as $response ):
							$hasImage = false; ?>
							<div class="response-box response-box-<?php echo $response['response_type']; ?>">
								<div class="response-box-content">
									<?php
									if($resImage = $response['response_image']) {
										$hasImage = true; ?>
										<div class="response-image">
											<a href="<?php echo $response['response_url']; ?>">
												<?php
												if ( $response['response_type'] != 'text' ) { ?>
													<div class="response-image-bg">
													</div>
												<?php
												} ?>
												<img class="response-image-image" src="<?php echo $resImage['sizes']['small-medium']; ?>"/>
												<img src="<?php echo get_template_directory_uri(); ?>/images/hover.png" class="hover-icon"/>
									<?php
									} ?>
											<div class="response-content">

												<div class="response-content-text">
													<?php
									        if($resText = $response['response_text']) {
														if($hasImage == 0) { ?>
															<a class="response-text" href="<?php echo $response['response_url']; ?>">
																<?php echo $resText; ?>
															</a>
														<?php
														} else { ?>
															<div class="response-text">
																<?php echo $resText; ?>
															</div>
														<?php
														}
													}
													if($resUser = $response['response_user']) { ?>
														<div class="response-user">
															<?php if($resUserImg = $response['response_user_image']) { ?>
																<div class="response-user-img bg-centered" style="background-image:url(<?php echo $resUserImg['sizes']['thumbnail']; ?>);">
																</div>
															<?php } ?>
															<span><?php echo $resUser; ?></span>
														</div>
													<?php
												} ?>
											</div>
										</div>
									<?php
									if($hasImage == 1) { // Close image div ?>
											</a>
										</div>
									<?php
									} ?>
								</div>
							</div>
						<?php
						endforeach; ?>
					</div>
				<?php
			endif; ?>
		</div>
		<?php
		endwhile;
		wp_reset_postdata();
		$postsObj->posts = ob_get_clean();
	endif;

	$checkArgs = $args;
	$checkArgs['paged'] = isset($checkArgs['paged']) ? $checkArgs['paged'] + 1 : 2;
	$checkNextQuery = new WP_Query( $checkArgs );
	$postsObj->more = false;
	if ( $checkNextQuery->have_posts() ) :
		$postsObj->more = true;
		wp_reset_postdata();
	endif;
	return $postsObj;
}

function beta_signup_form() {
	ob_start(); ?>

		<!-- <form id="beta-signup-form">
			<h1>Let's do this.</h1>
			<p>Patience, GrasshoppHer. We’re still in beta. Sign up now so you’re in-the-know when GrasshoppHer launches. Err, leaps.</p>
			<div id="beta-inputs">
				<input type="text" id="beta-name" placeholder="Full Name*" required>
				<input type="text" id="beta-email" placeholder="Email*" required>
				<input type="text" id="beta-age" placeholder="age">
				<input type="text" id="beta-school" placeholder="school">
			</div>
			<label for="beta-agree">
				<input id="beta-agree" type="checkbox">
				By clicking “sign up,” you agree to the GrasshoppHer <a target="_blank" href="<?php echo home_url('terms-conditions'); ?>">Terms and Conditions</a> and <a target="_blank" href="<?php echo home_url('privacy-policy'); ?>">Privacy Policy</a>.
			</label>
			<input id="beta-submit" type="submit" value="Sign up">
		</form> -->
	<?php
	echo do_shortcode('[ninja_form id=5]');
	echo ob_get_clean();
}

function signup_form( $atts ) {
  $a = shortcode_atts( array(), $atts );
  beta_signup_form();
}
add_shortcode( 'signup-form', 'signup_form' );
