<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="format-detection" content="date=no">
    <meta name="format-detection" content="address=no">
    <meta name="format-detection" content="telephone=no">
    <title>Email Template</title>

  <style type="text/css">
		[style*=Titillium Web]{
			font-family:'HelveticaNeue',sans-serif !important;
		}
		[style*=Benton Sans Cond]{
			font-family:'HelveticaNeue',sans-serif !important;
		}
		body{
			padding:0 !important;
			margin:0 !important;
			display:block !important;
			width:100% !important;
			background:#ffffff;
			-webkit-text-size-adjust:none;
		}
		a{
			color:#12283c;
			text-decoration:underline;
		}
		a:hover{
			color:white;
			background-color:gold;
			text-decoration:none;
		}
		p{
			padding:0 !important;
			margin:0 !important;
		}
    .social_share_button {
      width: 18px;
      height: auto;
      margin-left: 6px;
      display: inline-block;
      position:relative;
    }
    .social_share_button img{
      width: 100%;
      height: auto;
    }
	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		div[class=mobile-br-5]{
			height:5px !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		div[class=mobile-br-10]{
			height:10px !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		div[class=mobile-br-15]{
			height:15px !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		td[class=m-td],div[class=hide-for-mobile],span[class=hide-for-mobile]{
			display:none !important;
			width:0 !important;
			height:0 !important;
			font-size:0 !important;
			line-height:0 !important;
			min-height:0 !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		span[class=mobile-block]{
			display:block !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		div[class=wgmail] img{
			min-width:320px !important;
			width:320px !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		div[class=full-width-img] img{
			width:100% !important;
			max-width:480px !important;
			height:auto !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		div[class=content-width-img] img{
			width:100% !important;
			max-width:480px !important;
			height:auto !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		table[class=w320]{
			width:100% !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		td[class=column]{
			float:left !important;
			width:100% !important;
			display:block !important;
		}

}	@media only screen and (max-device-width: 480px),only screen and (max-width: 480px){
		td[class=content-spacing]{
			width:15px !important;
		}

}</style></head>
  <body class="body" style="padding:0 !important; margin:0 !important; display:block !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
      <tr>
        <td align="center" valign="top">
          <table width="630" border="0" cellspacing="0" cellpadding="0" class="w320">
            <tr>
              <td class="img" style="font-size:0pt;line-height:0pt;text-align:left;" width="15"></td>
              <td>
                <!-- Header -->
                <div style="font-size:0pt;line-height:0pt;height:40px;"></div>
                <div class="full-width-img" style="font-size:0pt;line-height:0pt;text-align:left;">
                  <a href="#" target="_blank"><img border="0" src="https://gallery.mailchimp.com/cf8fb76155baf1d37cdcafeb3/images/dee0bf2c-3fae-479a-bd22-76f8eba83560.jpg" mc:edit="image_1" alt="" width="600" style="max-width:600px" height="120"></a>
                </div>
                <div style="font-size:0pt;line-height:0pt;height:18px;"></div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="img" style="font-size:0pt;line-height:0pt;text-align:left;">
                      <div style="font-size:0pt;line-height:0pt;height:1px;background:#999999;"></div>
                    </td>
                    <td width="200" class="text-date" style="color:#000000;font-family:HelveticaNeue, sans-serif;font-size:20px;line-height:37px;text-align:center;">
                      <div mc:edit="text_1">
                        *|DATE:F j, Y|*
                      </div>
                    </td>
                    <td class="img" style="font-size:0pt;line-height:0pt;text-align:left;">
                      <div style="font-size:0pt;line-height:0pt;height:1px;background:#999999;"></div>
                    </td>
                  </tr>
                </table>
                <div style="font-size:0pt;line-height:0pt;height:15px;"></div>
                <!-- END Header -->
                <!-- Main -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                      <!-- Entry -->
                      <div mc:edit="entry_content">
                        <div>Content??</div>
                      </div>

                      <!-- END Entry -->

                      <!-- Headline -->
                      <div>
                        <div class="h2" style="color:#000000;font-family:HelveticaNeue, sans-serif;font-size:26px;line-height:36px;text-align:left;font-weight:bold;">


                          <div mc:edit="text_4">
                            <div>Signoff</div>
                          </div>
                        </div>

                      </div>
                      <div style="font-size:0pt;line-height:0pt;height:15px;"></div>
                      <!-- END Headline -->
                      <div mc:edit="extra_content">
                        <div style="color:#000000;font-family:HelveticaNeue, sans-serif;font-size:20px;text-align:left;font-style:italic;">optional extra section</div>
                      </div>
                      <div style="font-size:0pt;line-height:0pt;height:15px;"></div>
                    </td>
                  </tr>
                </table>
                <!-- END Main -->
                <!-- Footer -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                      <!-- <div class="img-center" style="font-size:0pt;line-height:0pt;text-align:center;">
                        <a href="http://loremipsum.wtf" target="_blank"><img border="0" src="https://gallery.mailchimp.com/cf8fb76155baf1d37cdcafeb3/images/fb02522b-c593-4e30-8022-c3d3bb9a6734.jpg" mc:edit="image_7" alt="" width="183" style="max-width:183px" height="49"></a>
                      </div> -->
                      <div class="h2" style="color:#000000;font-family:HelveticaNeue, sans-serif;font-size:26px;line-height:36px;text-align:left;font-weight:bold;margin-top:15px;">
                        Also of note: <a href="https://www.nytimes.com/2018/05/02/nyregion/summer-nyc-things-to-do.html"> Sign up</a> for my New York Times column!
                      </div>
                      <div class="h2" style="color:#000000;font-family:HelveticaNeue, sans-serif;font-size:26px;line-height:36px;text-align:left;font-weight:bold;margin-top:15px;">
                        Also also: share this link with your friends to get them on the Lorem list. <a href="https://loremipsum.wtf/?ref=*|EMAIL_UID|*"> https://loremipsum.wtf/?ref=*|EMAIL_UID|*</a>
                      </div>
                      <div style="font-size:0pt;line-height:0pt;height:28px;"></div>
                      <div class="text-footer" style="color:#000;font-family:Arial;font-size:12px;line-height:18px;text-align:center;">
                        <div mc:edit="text_5">
                          Copyright © 2018
                          <br>
                          Lorem Ipsum, All rights reserved.
                          <br>
                          P.O. Box 380011 Brooklyn, NY 11238
                        </div>
                      </div>
                      <div style="font-size:0pt;line-height:0pt;height:10px;"></div>
                      <div class="text-footer" style="font-family:HelveticaNeue;font-size:12px;line-height:18px;text-align:center;">
                        <div mc:edit="text_6">
                          <a target="_blank" href="*|UNSUB|*">Unsubscribe from this list</a>  |  <a target="_blank" href="*|UPDATE_PROFILE|*">Update subscription preferences</a>
                        </div>
                      </div>
                      <div style="font-size:0pt;line-height:0pt;height:20px;"></div>
                      <div class="img-center" style="font-size:0pt;line-height:0pt;text-align:center;">
                        <span style="color: #ffffff;">*|IF:REWARDS|* *|REWARDS|* *|END:IF|*</span>
                      </div>
                    </td>
                  </tr>
                </table>
                <div style="font-size:0pt;line-height:0pt;height:20px;"></div>
                <!-- END Footer -->
              </td>
              <td class="img" style="font-size:0pt;line-height:0pt;text-align:left;" width="15"></td>
            </tr>
          </table>
          <div class="wgmail" style="font-size:0pt;line-height:0pt;text-align:center;"><img src="https://gallery.mailchimp.com/cf8fb76155baf1d37cdcafeb3/images/a99280e0-c003-459b-9bb2-442fbf03e2d4.jpg" width="630" height="1" style="min-width:630px" alt="" border="0">
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>
