<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GrasshoppHer
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			if(has_post_thumbnail()) {
				$thumbnail = get_the_post_thumbnail_url( get_the_id(), 'large'); ?>
				<div class="content-inner">
					<div class="featured-image-container">
						<div class="featured-image">
							<img src="<?php echo $thumbnail; ?>"/>
						</div>
					</div>
				</div>
			<?php
			}
			if ( $diagram = get_field('diagram') ) { ?>
				<div class="content-inner">
					<div id="page-diagram">
						<?php
						foreach( $diagram as $d ) { ?>
							<div class="diagram-item">
								<?php
								if ( $d['image'] ) { ?>
									<div class="diagram-image-container">
										<div class="diagram-image" style="background-image:url(<?php echo $d['image']['sizes']['medium']; ?>);">
										</div>
									</div>
									<?php
								}
								if ( $d['text'] ) { ?>
									<div class="diagram-text">
										<?php echo $d['text']; ?>
									</div>
									<?php
								} ?>
							</div>
							<?php
						} ?>
					</div>
				</div>
			<?php
			} ?>
			<div class="content-inner content-section">
				<div class="section-left-indent">
					<div class="section-content">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
