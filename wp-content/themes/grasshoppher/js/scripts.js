import $ from 'jquery'

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top + ($(window).height() / 4);
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

jQuery(document).on('nfFormReady', function(form){
  $('.checkbox-wrap').each(function() {
    const $checkbox = this
    if ( !$('.checkbox-custom-check', $checkbox).length ) {
      $('.nf-field-element', $checkbox).append('<div class="checkbox-custom-check"></div>')
    }
  })
  styleSelect()
})

if($('body').hasClass('unverified')) {
  //document.body.addEventListener('touchstart', function(e){ e.preventDefault() });
}

// jQuery(document).on('nfFormReady', function() {
//   console.log("ninja form ready");
//   nfRadio.channel( 'nfMP' ).on('change:part', function() {
//     console.log("[nfMP] change:part");
//     // run your function here
//   });
// });

$(document).ready(function() {
  styleSelect()
  if($('.new-post').length) {
    showNewPosts(1);
  }
  if ( $('#gh-section-hero').length ) {
    $('#gh-section-hero p').each(function(i) {
      const $headerLine = $(this)
      console.log(i)
      setTimeout( function() {
        $headerLine.addClass('visible')
      }, 300 * i)
    })
  }
  if ( $('.gh-section').length ) {
    animateSections()
    $(window).scroll(function(){
      animateSections()
    })
  }
})

function animateSections() {
  $('.gh-section:not(.no-animate)').each( function() {
    const $animated = $(this);
    if ( !$animated.hasClass('visible') && $animated.isInViewport() ) {
      $animated.addClass('visible')
    }
  })
}

function handleQAClick(thisqa) {
  $('.share-qa', thisqa).each(function(q, qa){
    if($(thisqa).data('hashtags')){
      $(qa).on('click', function(e){
        e.preventDefault();
        var hashtags = $(thisqa).data('hashtags');
        var el = document.createElement('textarea');
        el.value = hashtags;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        window.open($(this).attr('href'), '_blank');
        // var $popup = $('<div style=display:none;" class="hashtag-popup"><div>Hashtags copied! Directing to <span style="text-transform:capitalize;">'+$(this).html()+'</span></div></div>');
        // $popup.insertBefore($('.question-social', thisqa));
        // $popup.slideDown('fast');
        // setTimeout(function(){
        //   $popup.slideUp('300');
        //   setTimeout(function(){
        //     $popup.remove();
        //     window.open($(this).attr('href'), '_blank');
        //   },300);
        // }, 500);
      });
    }
  })
}

$('#toggle-menu').on('click', function(){
  if ($('body').hasClass('menu-inactive') || $('body').hasClass('menu-active')) {
    $('body').toggleClass('menu-inactive');
    $('body').toggleClass('menu-active');
  }else {
    $('body').addClass('menu-active');
  }
});

var loading = false;

if($('.load-more-posts').length) {
  $('.load-more-posts').on('click', function(e) {
    e.preventDefault();
    if(!loading){
      var clickedEl = $(this);
      loadMorePosts(clickedEl, function(more){return});
    }
  });
}

function loadMorePosts($el, postsAdded) {
  loading = true;
  var paged = $($el).data('paged');
  var postCount = $($el).data('count');
  var postType = $($el).data('post-type');
  console.log(paged, postCount, postType);
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn' : 'get_more_posts',
      'paged' : paged,
      'count' : postCount,
      'type' : postType,
    }
  }).done( function (response) {
    //console.log(response);
    response = $.parseJSON(response);
    loading = false;
    console.log(response);
    if(response.posts) {
      $('.posts-container').append(response.posts);
      $($el).data('paged', paged+1);
      if(postType = 'qa'){
        var allItems = document.getElementsByClassName("response-box");
        for(var x=0;x<allItems.length;x++){
          imagesLoaded( allItems[x], resizeInstance);
        }
      }
      showNewPosts(paged);
      postsAdded(response.more);
    }
    if(!response.more) {
      $('.load-more-posts').hide();
    }
  });
}

function showNewPosts(paged) {
  imagesLoaded( $('.new-post'), function(){
    var scrollTo = '';
    $('.new-post').each(function(p, post) {
      if(p === 0) {
        scrollTo = $(post).offset().top;
      }
      let currentPost = post;
      $('.response-box a', post).on('click', function() {
        if($('body').hasClass('mobile') || $('body').hasClass('tablet')) {
          return false;
        }
      });
      handleQAClick(post);
      setTimeout(function(){
        $(post).removeClass('new-post');
      }, 100 * p);
    });
    if(paged != 1) {
      $('html, body').animate({
        scrollTop: scrollTo - 100
      }, 500);
    }
  });
}

function resizeGridItem(item){
  let grid = item.closest(".responses-container");
  let rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
  let rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
  console.log(rowGap, rowHeight, grid);
  let rowSpan = Math.ceil((item.querySelector('.response-box-content').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));
  item.style.gridRowEnd = "span "+rowSpan;
}
function resizeAllGridItems(){
  var allItems = document.getElementsByClassName("response-box");
  for(var x=0;x<allItems.length;x++){
    imagesLoaded( allItems[x], resizeInstance);
  }
}
function resizeInstance(instance){
  let item = instance.elements[0];
  resizeGridItem(item);
}
if($('.responses-container').length) {
  window.onload = resizeAllGridItems();
  window.addEventListener("resize", resizeAllGridItems);
  var allItems = document.getElementsByClassName("response-box");
  console.log(allItems);
  for(var x=0;x<allItems.length;x++){
    imagesLoaded( allItems[x], resizeInstance);
  }
}

if($('#submit-age-verify').length){
  let $day = document.getElementById('verify-day');
  $day.oninput = function () {
    if(this.value > 0) {
      $day.classList.remove("error");
    }
    if (this.value > 31) {
      this.value = '31';
    }
  }
  let $month = document.getElementById('verify-month');
  $month.oninput = function () {
    if(this.value > 0) {
      $month.classList.remove("error");
    }
    if (this.value > 12) {
      this.value = '12';
    }
  }
  let $year = document.getElementById('verify-year');
  $year.oninput = function () {
    if(this.value > 0) {
      $year.classList.remove("error");
    }
    if (this.value.length > 4) {
      this.value = this.value.slice(0,4);
    }
  }
}
$('#submit-age-verify').on('click', function(e) {
  $('#verify-terms').removeClass('error');
  var remember = $('#verify-remember').is(':checked') ? 1 : 0;
  var terms = $('#verify-terms').is(':checked') ? 1 : 0;
  var day = $('#verify-day').val();
  var month = $('#verify-month').val();
  var year = $('#verify-year').val();
  var dateFields = [$('#verify-day'), $('#verify-month'), $('#verify-year')];
  var birthday = '';
  var fullDate = true;
  $.each(dateFields, function(){
    if(!$(this).val()){
      $(this).addClass('error');
      fullDate = false;
    }else{
      birthday += $(this).val()+' ';
    }
  });
  if(fullDate && getAge(birthday) < 18 || !terms) {
    //Too young
    if(!terms) {
      $('#verify-terms').addClass('error');
    }
    return;
  }
  var bday = month+'/'+day;
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn' : 'check_age',
      'remember' : remember,
      'bday' : bday
    }
  }).done( function (response) {
    //console.log(response);
    response = $.parseJSON(response);
    if(response.age && response.country) {
      $('body').removeClass('unverified');
    } else {
      if(!response.age) {
        // age is wrong
      }
      if(!response.country) {
        // wrong country
      }
    }
  });
});

function getAge(DOB) {
  var today = new Date();
  var birthDate = new Date(DOB);
  console.log(birthDate);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }
  return age;
}

function styleSelect() {
  var x, i, j, selElmnt, a, b, c;
  /*look for any elements with the class "custom-select":*/
  x = document.getElementsByClassName("custom-select");
  console.log(x)
  for (i = 0; i < x.length; i++) {
    console.log(i)
    if($('.select-selected', x[i]).length == 0) {
      console.log('styling select', x[i])
      selElmnt = x[i].getElementsByTagName("select")[0];
      console.log(selElmnt)
      /*for each element, create a new DIV that will act as the selected item:*/
      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
      x[i].appendChild(a);
      /*for each element, create a new DIV that will contain the option list:*/
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
      for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
              if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                y = this.parentNode.getElementsByClassName("same-as-selected");
                for (k = 0; k < y.length; k++) {
                  y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
              }
            }
            h.click();
        });
        b.appendChild(c);
      }
      x[i].appendChild(b);
      a.addEventListener("click", function(e) {
          /*when the select box is clicked, close any other select boxes,
          and open/close the current select box:*/
          e.stopPropagation();
          closeAllSelect(this);
          this.nextSibling.classList.toggle("select-hide");
          this.classList.toggle("select-arrow-active");
      });
    }
  }
  function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
      if (elmnt == y[i]) {
        arrNo.push(i)
      } else {
        y[i].classList.remove("select-arrow-active");
      }
    }
    for (i = 0; i < x.length; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
      }
    }
  }
  /*if the user clicks anywhere outside the select box,
  then close all select boxes:*/
  document.addEventListener("click", closeAllSelect);
}

if($('#beta-submit').length){
  $('#beta-submit').on('click', function(e) {
    e.preventDefault();
    $('#beta-agree').removeClass('error');
    $('#beta-inputs input').removeClass('error');
    var $name = $('#beta-name');
    var $email = $('#beta-email');
    var $age = $('#beta-age');
    var $school = $('#beta-school');
    var signupdata = [$name, $email, $age, $school];
    var missingFields = false;
    $.each(signupdata, function(i, input) {
      if(!$(input).val().length) {
        $(input).addClass('error');
        missingFields = true;
      }
    });
    if(!$('#beta-agree').is(':checked') ? 1 : 0) {
      $('#beta-agree').addClass('error');
      missingFields = true;
    }
    if(!missingFields && !isEmail($email.val())) {
      $($email).addClass('error');
      missingFields = true;
    }
    if(!missingFields) {
      signup_user($($name).val(), $($email).val(), $($age).val(), $($school).val());
    }
  });
}

function signup_user(name, email, age, school) {
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn' : 'sign_up_user',
      'name' : name,
      'email' : email,
      'age' : parseInt(age),
      'school' : school
    }
  }).done( function (response) {
    response = $.parseJSON(response);
    console.log(response);
    if($('#form-container .error-response').length) {
      $('#form-container .error-response').remove();
    }
    if(response.status === 'successful signup') {
      $('#beta-signup-form').html(`<h2>${response.response}</h2>`);
    } else {
      $('#beta-signup-form').prepend(`
        <div class="error-response">
          ${response.response}
        </div>`
      )
    }
    //$('.li-signup-text').html('<div class="response-text">'+response.response+'</div>');
  });
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
