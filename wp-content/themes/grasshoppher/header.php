<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GrasshoppHer
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0"/>
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
	  (adsbygoogle = window.adsbygoogle || []).push({
	    google_ad_client: "ca-pub-9878615085984225",
	    enable_page_level_ads: true
	  });
	</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'grasshoppher' ); ?></a> -->

	<header id="masthead" class="site-header">
		<div class="content-inner">

			<div class="site-branding">
				<?php the_custom_logo(); ?>
			</div><!-- .site-branding -->
			
			<nav class="main-navigation" role="navigation">
				<?php wp_nav_menu( array('menu' => 'header-menu-left') );?>
				<?php the_custom_logo(); ?>
				<?php wp_nav_menu( array('menu' => 'header-menu-right') ); ?>
			</nav>
			<!-- #site-navigation -->
		</div>
		<div id="toggle-menu">
			<div class="toggle-line"></div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
