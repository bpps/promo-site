<?php
/**
 * Template Name:  Mentors Page
 * The template for displaying all pages
 *
 * Template for Mentors page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GrasshoppHer
 */
 get_header();
 ?>

 	<div id="primary" class="content-area">
 		<main id="main" class="site-main">
      <div class="content-inner">
     		<?php
     		while ( have_posts() ) :
     			the_post(); ?>
          <h1 class="section-header"><?php the_title(); ?></h1>
          <?php the_content(); ?>
        <?php
     		endwhile; // End of the loop.
        $mentorCount = 9;
        $mentorArgs = [
      		'post_type' => 'mentor',
      		'posts_per_page' => $mentorCount,
      	];
     		?>
        <div id="mentors-container" class="posts-container gh-section">
          <?php
          $mentors = get_mentors($mentorArgs);
          echo $mentors->posts; ?>
        </div>
        <?php
        if($mentors->more) { ?>
          <a
            class="drk-pink-btn load-more-posts"
            data-count="<?php echo $mentorCount; ?>"
            data-paged="2"
            data-post-type="mentor">
            Show More
          </a>
        <?php } ?>
        <div class="gh-section">
          <section id="form-container">
            <div class="form-container-content">
              <?php echo do_shortcode('[ninja_form id=4]'); ?>
            </div>
          </section>
        </div>
      </div>

 		</main><!-- #main -->
 	</div><!-- #primary -->

 <?php
 get_footer(); ?>
