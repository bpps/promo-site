<?php
/**
 * Template Name: Contact Page
 * The template for displaying all pages
 *
 * Template for Contact page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GrasshoppHer
 */
 get_header();
 ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section id="form-container">
				<?php echo do_shortcode('[ninja_form id=8]'); ?>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
