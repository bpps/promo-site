<?php
/**
 * Template Name:  Q+A Page
 * The template for displaying Q + A page
 *
 * Template for Q+A page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GrasshoppHer
 */
 get_header();
 ?>

 	<div id="primary" class="content-area">
 		<main id="main" class="site-main">
     		<?php
     		while ( have_posts() ) :
     			the_post();
          $qaCount = 2;
          $qaArgs = [
        		'post_type' => 'qa',
        		'posts_per_page' => $qaCount,
            'paged' => 1,
            'post_status' => 'publish',
        	];
       		?>
          <div class="gh-section">
            <div class="content-inner">
              <div class="section-qa">
                <div class="section-content">
                  <h1 class="section-header"><?php the_title(); ?></h1>
                  <?php
                  the_content();
                  $qas = get_qas($qaArgs);
                  if($qas->posts) { ?>
                  <div id="qas-container" class="posts-container">
                    <?php
                    echo $qas->posts; ?>
                  </div>
                  <?php
                  }
                  if($qas->more) { ?>
                    <div class="btn-container">
                      <a
                        class="blue-btn load-more-posts"
                        data-count="<?php echo $qaCount; ?>"
                        data-paged="2"
                        data-post-type="qa">
                        More Questions
                      </a>
                    </div>
                  <?php
                  } ?>
                  <section id="form-container">
                    <div class="form-container-content">
                      <?php echo do_shortcode('[ninja_form id=2]'); ?>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>
        <?php
        endwhile; // End of the loop. ?>

 		</main><!-- #main -->
 	</div><!-- #primary -->
 <?php
 get_footer(); ?>
