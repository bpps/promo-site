<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GrasshoppHer
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<?php
		$homeID = get_option('page_on_front');
		$heroText = get_field('hero_text', $homeID);
		$aboutText = get_field('about_text', $homeID);
		$qaHeader = get_field('question_header_text', $homeID);
		?>
		<div id="gh-section-hero" class="gh-section no-animate">
			<div class="content-inner">
				<div class="section-left">
					<div class="section-content">
						<?php
						print $heroText;
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
		if ( $content_areas = get_field('content_areas') ) {
			foreach ( $content_areas as $content ) { ?>
				<div class="gh-section">
					<div class="content-inner">
						<?php
						$layout = $content['layout']; ?>
						<div class="section-wrapper flex <?php echo $layout; ?>">
							<?php
							if ( $content['image'] ) { ?>
								<div class="section-image">
									<img src="<?php echo $content['image']['sizes']['medium']; ?>"/>
								</div>
							<?php
							}
							if ( isset($content['content']) ) {
								$p_content = preg_match_all('|<p>(.*?)</p>|', $content['content'], $output);
								$p_length = strlen($output[0][0]); ?>
								<div class="section-content-wrapper<?php echo $p_length > 250 ? ' small-text' : ''; ?>">
									<?php
									if ( $content_title = $content['title'] ) { ?>
										<h2><?php echo $content_title; ?></h2>
									<?php
									} ?>
									<div class="section-content">
										<?php echo $content['content']; ?>
										<?php
										if ( $content['link'] ) { ?>
											<a class="arrow-link" href="<?php echo $content['link']['url']; ?>">
												<?php echo isset($content['link']['title']) ? $content['link']['title'] : 'Read More'; ?>
											</a>
										<?php
										} ?>
									</div>
								</div>
							<?php
							} ?>
						</div>
					</div>
				</div>
			<?php
			}
		} ?>
		<section id="background-side_3-wrapper">
			<div id="gh-section-questions" class="gh-section">
				<div class="content-inner">
					<div class="section-content">
						<div class="section-content-split">
							<?php qa_preview(); ?>
							<?php mentor_preview(); ?>
						</div>
					</div>
				</div>
			</div>
			<div id="gh-section-form" class="gh-section">
				<div class="content-inner">
					<div class="section-content">
						<section id="form-container">
							<?php beta_signup_form(); ?>
						</section>
					</div>
				</div>
			</div>
		</section>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
