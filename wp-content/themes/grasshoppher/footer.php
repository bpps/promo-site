<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GrasshoppHer
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer">
			<div class="footer-content-wrapper">
				<img class="mobile footer-image" src="<?php echo get_template_directory_uri(); ?>/images/footer-mobile.png"/>
				<div class="footer-content">
					<?php
					wp_nav_menu( ['menu' => 'footer-menu']);
					gh_social_icons(); ?>
				</div>
			</div>
			<div class="footer-image">
			</div>
	</footer><!-- #colophon -->

	<?php
	$crawler = crawlerDetect($_SERVER['HTTP_USER_AGENT']);
	// To enable: remove 2==3
	if( 2 == 3 && !$crawler && !isset($_COOKIE['age-verified'])) { ?>
		<section id="age-verify-wrapper">
			<div id="age-verify">
				<div id="age-verify-content">
					<?php echo do_shortcode('[ninja_forms id=7]'); ?>
					<div id="age-verify-links">
						<?php wp_nav_menu( array('menu' => 'popup-menu') ); ?>
					</div>
				</div>
			</div>
		</section>

	<?php
	} ?>
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
var myCustomController = Marionette.Object.extend({

initialize: function() {
	formID = 7;
	Backbone.Radio.channel( 'form-' + formID ).reply( 'maybe:submit', this.beforeSubmit, this, formID );
},

beforeSubmit: function( formID ) {

		var formModel = nfRadio.channel( 'app' ).request( 'get:form', formID );
		var month = jQuery('#nf-field-42-container .select-selected').html();
		var monthnum = jQuery('#nf-field-42').val();
		var day = jQuery('#nf-field-69-container .select-selected').html();
		var year = jQuery('#nf-field-71-container .select-selected').html();
		var age = getAge(`${month} ${day}, ${year}`);
		console.log(month, day, year)
		if ( day === 'Day' || year === 'Year' ) {
			return false
		}
		if(age < 13) {
			// Not old enough
			console.log(jQuery('#nf-field-53-container input').val())
			jQuery('.age-verify-response').html('Aw, you have to be at least 13 to use GrasshoppHer, but we hope to see you soon!');
			jQuery('#nf-field-53-container').show()
			if ( !jQuery('#nf-field-53-container input').hasClass('nf-checked') ) {
				return false;
			}

		}
		jQuery.ajax({
			url: ajaxurl,
			method: 'post',
			type: 'json',
			data: {
				'action': 'do_ajax',
				'fn' : 'check_age',
				'bday' : `${monthnum}/${day}`
			}
		}).done( function (response) {
			//console.log(response);
			response = jQuery.parseJSON(response);
			if(response.age && response.country) {
				jQuery('body').removeClass('unverified');
				//document.body.removeEventListener('touchstart');
			} else {
				if(!response.age) {
					// age is wrong
				}
				if(!response.country) {
					// wrong country
				}
			}
		});
		return true;
		this.otherProcessing( formModel );

		// Halt form submission.
		return false;
},

otherProcessing: function( formModel ) {
	console.log(formModel);

	// Set re-start flag
	nfRadio.channel( 'form-' + formModel.get( 'id' ) ).request( 'add:extra', 'my_restart_flag', true );


	// Re-start submission.
	nfRadio.channel( 'form-' + formModel.get( 'id' ) ).request( 'submit', formModel );
},

});

function getAge(DOB) {
	var today = new Date();
	var birthDate = new Date(DOB);

	var age = today.getFullYear() - birthDate.getFullYear();
	var m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
	}
	return age;
}

jQuery( document ).ready( function( $ ) {
	if($('#age-verify-wrapper').length) {
		new myCustomController();
	}
});
</script>
</body>
</html>
